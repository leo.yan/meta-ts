Introduction
------------
This repository contains the Linaro Trustedsubstrate layers for OpenEmbedded.

Building
--------

Make sure kas is installed on your system
```kas build ci/<platform.yaml>```

Supported platforms:
- qemuarm64-secureboot
- synquacer
- stm32mp157c-dk2
- stm32mp157c-ev1
- rockpi4b
- rpi4
- zynqmp-starter

For more information on using the OpenEmbedded layer look at 
https://trs.readthedocs.io/en/latest/firmware/index.html

CI
--

https://gitlab.com/Linaro/blueprints/ci/#arm-blueprints-ci
