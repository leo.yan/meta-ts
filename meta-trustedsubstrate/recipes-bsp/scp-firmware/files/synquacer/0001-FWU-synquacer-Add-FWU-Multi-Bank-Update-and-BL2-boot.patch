From a1f9172300e5cc3379773b7816b5a6793d35ccd1 Mon Sep 17 00:00:00 2001
From: Masami Hiramatsu <masami.hiramatsu@linaro.org>
Date: Tue, 19 Apr 2022 22:57:58 +0900
Subject: [PATCH] FWU: synquacer: Add FWU Multi Bank Update and BL2 boot
 support

Add FWU Multi Bank Update and boot BL2 from FIP image support for
SynQuacer platform.

When the DSW 3-4 is on, SynQuacer platform will decode FWU metadata
and platform metadata to choose the active bank of the FIP image
and boot BL2 from it.

This also enables the platform trial boot, which count up the boot
counter in the platform metadata, and when it reaches to the limit
(== 3) it switch back to the previous active bank. This counter will
stop when the BL33 (typically U-Boot) clears the counter.

Signed-off-by: Jassi Brar <jaswinder.singh@linaro.org>
Signed-off-by: Masami Hiramatsu <masami.hiramatsu@linaro.org>
Signed-off-by: Masahisa Kojima <masahisa.kojima@linaro.org>
Change-Id: I00b9c932fac5b4a9a231fcf2a050b766abefed61
---
 product/synquacer/include/efi.h               | 15 +++
 product/synquacer/include/fwu_mdata.h         | 74 ++++++++++++++
 product/synquacer/include/synquacer_mmap.h    | 11 +++
 .../synquacer_system/src/load_secure_fw.c     | 67 ++++++++++++-
 .../synquacer_system/src/synquacer_main.c     | 97 ++++++++++++++++++-
 5 files changed, 259 insertions(+), 5 deletions(-)
 create mode 100644 product/synquacer/include/efi.h
 create mode 100644 product/synquacer/include/fwu_mdata.h

diff --git a/product/synquacer/include/efi.h b/product/synquacer/include/efi.h
new file mode 100644
index 00000000..e7a49852
--- /dev/null
+++ b/product/synquacer/include/efi.h
@@ -0,0 +1,15 @@
+/*
+ * Arm SCP/MCP Software
+ * Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
+ *
+ * SPDX-License-Identifier: BSD-3-Clause
+ */
+
+#ifndef SYNQUACER_EFI_H
+#define SYNQUACER_EFI_H
+
+typedef struct {
+        uint8_t b[16];
+} efi_guid_t __attribute__((aligned(8)));
+
+#endif /* SYNQUACER_EFI_H */
diff --git a/product/synquacer/include/fwu_mdata.h b/product/synquacer/include/fwu_mdata.h
new file mode 100644
index 00000000..12bce073
--- /dev/null
+++ b/product/synquacer/include/fwu_mdata.h
@@ -0,0 +1,74 @@
+/*
+ * Arm SCP/MCP Software
+ * Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
+ *
+ * SPDX-License-Identifier: BSD-3-Clause
+ */
+
+#ifndef FWU_MDATA_H
+#define FWU_MDATA_H
+
+#include <efi.h>
+
+/*!
+ * \brief firmware image information
+ *
+ * \details The structure contains image specific fields which are
+ * used to identify the image and to specify the image's
+ * acceptance status
+ */
+struct fwu_image_bank_info {
+    /*! Guid value of the image in this bank */
+    efi_guid_t  image_uuid;
+
+    /*! Acceptance status of the image */
+    uint32_t accepted;
+
+    /*! Reserved */
+    uint32_t reserved;
+} __attribute__((__packed__));
+
+/*!
+ * \brief information for a particular type of image
+ *
+ * \details This structure contains information on various types of updatable
+ * firmware images. Each image type then contains an array of image
+ * information per bank.
+ */
+struct fwu_image_entry {
+    /*! Guid value for identifying the image type */
+    efi_guid_t image_type_uuid;
+
+    /*! Guid of the storage volume where the image is located */
+    efi_guid_t location_uuid;
+
+    /*! Array containing properties of images */
+    struct fwu_image_bank_info img_bank_info[CONFIG_FWU_NUM_BANKS];
+} __attribute__((__packed__));
+
+/*!
+ * \brief FWU metadata structure for multi-bank updates
+ *
+ * \details This structure is used to store all the needed information for
+ * performing multi bank updates on the platform. This contains info on the
+ * bank being used to boot along with the information needed for
+ * identification of individual images.
+ */
+struct fwu_mdata {
+    /*! crc32 value for the FWU metadata */
+    uint32_t crc32;
+
+    /*! FWU metadata version */
+    uint32_t version;
+
+    /*! Index of the bank currently used for booting images */
+    uint32_t active_index;
+
+    /*! Index of the bank used before the current bank being used for booting */
+    uint32_t previous_active_index;
+
+    /*! Array of information on various firmware images that can be updated */
+    struct fwu_image_entry img_entry[CONFIG_FWU_NUM_IMAGES_PER_BANK];
+} __attribute__((__packed__));
+
+#endif /* FWU_MDATA_H */
diff --git a/product/synquacer/include/synquacer_mmap.h b/product/synquacer/include/synquacer_mmap.h
index f0aaa843..9dfed5d5 100644
--- a/product/synquacer/include/synquacer_mmap.h
+++ b/product/synquacer/include/synquacer_mmap.h
@@ -172,6 +172,17 @@
                                      HSSPI_MEM_BASE)
 #define CONFIG_SCB_UEFI_BASE_ADDR UINT32_C(0xA8200000)
 
+/* FWU and platform metadata address */
+#define CONFIG_SCB_FWU_METADATA_OFFS        UINT32_C(0x500000)
+#define CONFIG_SCB_PLAT_METADATA_OFFS       UINT32_C(0x510000)
+#define CONFIG_SCB_FWU_BANK_SIZE            UINT32_C(0x400000)
+#define CONFIG_FWU_NUM_IMAGES_PER_BANK      1
+#define CONFIG_FWU_NUM_BANKS                2
+#define CONFIG_FWU_MAX_COUNT                3
+
+/* TBBR supported new FIP image offset */
+#define CONFIG_SCB_ARM_BL2_OFFSET           UINT32_C(0x600000)
+
 #define CONFIG_SCB_ARM_TB_BL1_BASE_ADDR UINT32_C(0xA4000000)
 #define CONFIG_SCB_ARM_TB_BL2_BASE_ADDR UINT32_C(0xA4013000)
 #define CONFIG_SCB_ARM_TB_BL3_BASE_ADDR UINT32_C(0xA401F000)
diff --git a/product/synquacer/module/synquacer_system/src/load_secure_fw.c b/product/synquacer/module/synquacer_system/src/load_secure_fw.c
index 821c1c77..17ee81bf 100644
--- a/product/synquacer/module/synquacer_system/src/load_secure_fw.c
+++ b/product/synquacer/module/synquacer_system/src/load_secure_fw.c
@@ -64,6 +64,20 @@ typedef struct arm_tf_fip_package_s arm_tf_fip_package_t;
 #define SCP_ADDR_TRANS_AREA UINT32_C(0xCB000000)
 #define BL32_TOC_ENTRY_INDEX (3)
 
+#define FIP_TOC_HEADER_NAME UINT32_C(0xAA640001)
+
+#define UUID_NULL                               \
+    {                                           \
+        0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, \
+        0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0  \
+    }
+
+#define FIP_UUID_TFA_BL2                                \
+    {                                                   \
+        0x5f, 0xf9, 0xec, 0x0b, 0x4d, 0x22, 0x3e, 0x4d, \
+        0xa5, 0x44, 0xc3, 0x9d, 0x81, 0xc7, 0x3f, 0x0a, \
+    }
+
 #define UUID_SECURE_PAYLOAD_BL32                                          \
     {                                                                     \
         0x05, 0xd0, 0xe1, 0x89, 0x53, 0xdc, 0x13, 0x47, 0x8d, 0x2b, 0x50, \
@@ -71,7 +85,58 @@ typedef struct arm_tf_fip_package_s arm_tf_fip_package_t;
     }
 
 /*
- * Current implementation expects bl32 is located as 4th binary
+ * New FIP loader for TBBR supported BL2
+ */
+void fw_fip_load_bl2(uint32_t boot_index)
+{
+    fip_toc_entry_t *toc_entry;
+    unsigned long offset;
+    uint8_t uuid_null[] = UUID_NULL;
+    uint8_t uuid_bl2[] = FIP_UUID_TFA_BL2;
+
+    arm_tf_fip_package_t fip_package;
+
+    offset = CONFIG_SCB_ARM_BL2_OFFSET + boot_index * CONFIG_SCB_FWU_BANK_SIZE;
+
+    synquacer_system_ctx.nor_api->read(
+        nor_id,
+        0,
+        MOD_NOR_READ_FAST_1_4_4_4BYTE,
+        offset,
+        &fip_package,
+        sizeof(fip_package));
+
+    if (fip_package.fip_toc_header.name != FIP_TOC_HEADER_NAME) {
+        FWK_LOG_ERR("No FIP Image found @%lx !", offset);
+        return;
+    }
+
+    toc_entry = fip_package.fip_toc_entry;
+
+    do {
+        if (memcmp((void *) uuid_null, (void *)toc_entry->uuid, 16) == 0) {
+            FWK_LOG_ERR("[FIP] BL2 not found!");
+            return;
+        }
+        if (memcmp((void *)uuid_bl2, (void *)toc_entry->uuid, 16) == 0) {
+            offset += (uint32_t)toc_entry->offset_addr;
+            FWK_LOG_INFO("[FIP] BL2 found %ubytes @%lx ",
+                         (unsigned int)toc_entry->size, offset);
+            synquacer_system_ctx.nor_api->read(
+                nor_id,
+                0,
+                MOD_NOR_READ_FAST_1_4_4_4BYTE,
+                offset,
+                (void *)CONFIG_SCB_ARM_TB_BL1_BASE_ADDR,
+                toc_entry->size);
+            return;
+        }
+        toc_entry++;
+    } while (1);
+}
+
+/*
+ * Legacy implementation expects bl32 is located as 4th binary
  * in the arm-tf fip package.
  */
 static void fw_fip_load_bl32(arm_tf_fip_package_t *fip_package_p)
diff --git a/product/synquacer/module/synquacer_system/src/synquacer_main.c b/product/synquacer/module/synquacer_system/src/synquacer_main.c
index fe1505d9..067a2a8b 100644
--- a/product/synquacer/module/synquacer_system/src/synquacer_main.c
+++ b/product/synquacer/module/synquacer_system/src/synquacer_main.c
@@ -13,6 +13,7 @@
 #include "synquacer_common.h"
 #include "synquacer_config.h"
 #include "synquacer_mmap.h"
+#include "fwu_mdata.h"
 
 #include <boot_ctl.h>
 #include <sysdef_option.h>
@@ -61,6 +62,7 @@ static fwk_id_t nor_id = FWK_ID_ELEMENT(FWK_MODULE_IDX_NOR, 0);
 void power_domain_coldboot(void);
 int fw_ddr_spd_param_check(void);
 void bus_sysoc_init(void);
+void fw_fip_load_bl2(uint32_t boot_index);
 void fw_fip_load_arm_tf(void);
 void smmu_wrapper_initialize(void);
 void pcie_wrapper_configure(void);
@@ -368,14 +370,101 @@ void main_initialize(void)
     return;
 }
 
+/* FWU platform metadata for SynQuacer */
+struct fwu_synquacer_metadata {
+    uint32_t boot_index;
+    uint32_t boot_count;
+} __attribute__((__packed__));
+
+static void update_platform_metadata(struct fwu_synquacer_metadata *platdata)
+{
+    struct fwu_synquacer_metadata buf;
+
+    synquacer_system_ctx.nor_api->read(nor_id, 0,
+                                       MOD_NOR_READ_FAST_1_4_4_4BYTE,
+                                       CONFIG_SCB_PLAT_METADATA_OFFS,
+                                       &buf, sizeof(buf));
+
+    if (!memcmp(platdata, &buf, sizeof(buf))) {
+        return;
+    }
+
+    synquacer_system_ctx.nor_api->erase(nor_id, 0,
+                                        MOD_NOR_ERASE_BLOCK_4BYTE,
+                                        CONFIG_SCB_PLAT_METADATA_OFFS,
+                                        sizeof(*platdata));
+    synquacer_system_ctx.nor_api->program(nor_id, 0,
+                                          MOD_NOR_PROGRAM_4BYTE,
+                                          CONFIG_SCB_PLAT_METADATA_OFFS,
+                                          platdata, sizeof(*platdata));
+    /* Read to verify and set the "read" command-sequence */
+    synquacer_system_ctx.nor_api->read(nor_id, 0,
+                                       MOD_NOR_READ_FAST_1_4_4_4BYTE,
+                                       CONFIG_SCB_PLAT_METADATA_OFFS,
+                                       &buf, sizeof(buf));
+    if (memcmp(platdata, &buf, sizeof(buf))) {
+        FWK_LOG_ERR("[FWU] Failed to update boot-index!\n");
+    }
+}
+
+static uint32_t fwu_plat_get_boot_index(void)
+{
+    struct fwu_synquacer_metadata platdata;
+    struct fwu_mdata metadata;
+
+    /* Read metadata */
+    synquacer_system_ctx.nor_api->read(nor_id, 0,
+                                       MOD_NOR_READ_FAST_1_4_4_4BYTE,
+                                       CONFIG_SCB_FWU_METADATA_OFFS,
+                                       &metadata, sizeof(metadata));
+
+    synquacer_system_ctx.nor_api->read(nor_id, 0,
+                                       MOD_NOR_READ_FAST_1_4_4_4BYTE,
+                                       CONFIG_SCB_PLAT_METADATA_OFFS,
+                                       &platdata, sizeof(platdata));
+
+    /* TODO: use CRC32 */
+    if (metadata.version != 1 ||
+        metadata.active_index > CONFIG_FWU_NUM_BANKS) {
+            platdata.boot_index = 0;
+            FWK_LOG_ERR("[FWU] FWU metadata is broken. Use default boot indx 0\n");
+    } else if (metadata.active_index != platdata.boot_index) {
+        /* Switch to new active bank as a trial. */
+        platdata.boot_index = metadata.active_index;
+        platdata.boot_count = 1;
+        FWK_LOG_INFO("[FWU] New firmware will boot. New index is %d\n",
+        (int)platdata.boot_index);
+    } else if (platdata.boot_count) {
+        /* BL33 will clear the boot_count when boot. */
+        if (platdata.boot_count < CONFIG_FWU_MAX_COUNT) {
+            platdata.boot_count++;
+    } else {
+            platdata.boot_index = metadata.previous_active_index;
+            platdata.boot_count = 0;
+            FWK_LOG_ERR("[FWU] New firmware boot trial failed. Rollback index is %d\n",
+                        (int)platdata.boot_index);
+        }
+    }
+
+    update_platform_metadata(&platdata);
+
+    return platdata.boot_index;
+}
+
 static void fw_wakeup_ap(void)
 {
     ap_dev_init();
 
-    FWK_LOG_INFO("[SYNQUACER SYSTEM] Arm tf load start.");
-    fw_fip_load_arm_tf();
-    FWK_LOG_INFO("[SYNQUACER SYSTEM] Arm tf load end.");
-
+    /* Check DSW 3-4 */
+    if (gpio_get_data((void *)CONFIG_SOC_AP_GPIO_BASE, 0) & 0x8) {
+        FWK_LOG_INFO("[SYNQUACER SYSTEM] Arm tf BL2 load start.");
+        fw_fip_load_bl2(fwu_plat_get_boot_index());
+        FWK_LOG_INFO("[SYNQUACER SYSTEM] Arm tf BL2 load end.");
+    } else {
+        FWK_LOG_INFO("[SYNQUACER SYSTEM] Arm tf load start.");
+        fw_fip_load_arm_tf();
+        FWK_LOG_INFO("[SYNQUACER SYSTEM] Arm tf load end.");
+    }
     synquacer_system_ctx.nor_api->configure_mmap_read(
         nor_id, 0, MOD_NOR_READ_FAST_1_4_4_4BYTE, true);
 }
-- 
2.17.1

