# Socionext Synquacer 64-bit machines specific TFA support

COMPATIBLE_MACHINE = "synquacer"

FILESEXTRAPATHS:prepend := "${THISDIR}/files/synquacer:"

SRC_URI += "\
        file://0001-refactor-synquacer-move-common-source-files.patch \
        file://0002-feat-synquacer-add-BL2-support.patch \
        file://0003-feat-synquacer-add-TBBR-support.patch \
        file://0004-feat-synquacer-add-FWU-Multi-Bank-Update-support.patch \
        file://0005-fix-synquacer-increase-size-of-BL33.patch \
"

TFA_DEBUG = "1"
TFA_UBOOT = "1"
TFA_MBEDTLS = "1"
TFA_BUILD_TARGET = "all fip"
TFA_INSTALL_TARGET = "bl2 fip.bin"

TFA_TARGET_PLATFORM = "synquacer"

# Enabling Secure-EL1 Payload Dispatcher (SPD)
TFA_SPD = "opteed"
# Cortex-A35 supports Armv8.0-A (no S-EL2 execution state).
# So, the SPD SPMC component should run at the S-EL1 execution state
TFA_SPMD_SPM_AT_SEL2 = "0"

# BL2 loads BL32 (optee). So, optee needs to be built first:
DEPENDS += " optee-os scp-firmware"

EXTRA_OEMAKE += 'SQ_USE_SCMI_DRIVER=1'

EXTRA_OEMAKE += "\
                        ARCH=aarch64 \
                        TARGET_PLATFORM=${TFA_TARGET_PLATFORM} \
                        TRUSTED_BOARD_BOOT=1 GENERATE_COT=1 \
                        ARM_ROTPK_LOCATION=devel_rsa  \
                        BL32=${RECIPE_SYSROOT}/lib/firmware/tee-pager_v2.bin \
			"
do_deploy:append() {
    echo "deploy synquacer"
    cd ${DEPLOYDIR}
    echo ${STAGING_DIR_TARGET}
    ${S}/tools/fiptool/fiptool update --tb-fw ${D}/firmware/bl2.bin fip.bin
    cd -
}

TFA_FWUPD_BIN = "${DEPLOYDIR}/fip.bin"

addtask deploy before do_build after do_compile

FILES:${PN} = "/boot /firmware"
