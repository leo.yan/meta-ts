# Machine specific TFAs

FILESEXTRAPATHS:prepend := "${THISDIR}/files/:"

SRC_URI = "git://git.trustedfirmware.org/TF-A/trusted-firmware-a.git;protocol=https;name=tfa;branch=master"
SRCREV_tfa = "35f4c7295bafeb32c8bcbdfb6a3f2e74a57e732b"
LIC_FILES_CHKSUM="file://docs/license.rst;md5=b2c740efedc159745b9b31f88ff03dde"

# mbed TLS v2.28.0
SRC_URI_MBEDTLS = "git://github.com/ARMmbed/mbedtls.git;name=mbedtls;protocol=https;destsuffix=git/mbedtls;branch=mbedtls-2.28"
SRCREV_mbedtls = "8b3f26a5ac38d4fdccbc5c5366229f3e01dafcc0"
LIC_FILES_CHKSUM_MBEDTLS = "file://mbedtls/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"
TFA_MBEDTLS = "1"

EXTRA_OEMAKE += " LOG_LEVEL=30"

MACHINE_TFA_REQUIRE ?= ""

MACHINE_TFA_REQUIRE:synquacer = "trusted-firmware-a-synquacer.inc"
MACHINE_TFA_REQUIRE:stm32mp157c-dk2 = "trusted-firmware-a-stm32mp157c-dk2.inc"
MACHINE_TFA_REQUIRE:stm32mp157c-ev1 = "trusted-firmware-a-stm32mp157c-ev1.inc"
MACHINE_TFA_REQUIRE:rockpi4b = "trusted-firmware-a-rockpi4b.inc"
MACHINE_TFA_REQUIRE:zynqmp-starter = "trusted-firmware-a-zynqmp.inc"
MACHINE_TFA_REQUIRE:zynqmp-production = "trusted-firmware-a-zynqmp.inc"
MACHINE_TFA_REQUIRE:tsqemuarm64-secureboot = "trusted-firmware-a-tsqemuarm64-secureboot.inc"

require ${MACHINE_TFA_REQUIRE}
