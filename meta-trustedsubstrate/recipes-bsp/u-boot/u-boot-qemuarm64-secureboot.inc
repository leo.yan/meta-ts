# Temporarily disable capsule creation until upstream u-boot is fixed
#require u-boot-capsule.inc

SRC_URI += "\
    file://qemu_arm64_defconfig \
    file://qemu/boot_opt.var \
    file://qemu/boot_order.var \
"
